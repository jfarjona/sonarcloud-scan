# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.5

- patch: Standardise pipe variable validation messages.

## 0.1.4

- patch: Switched to JRE; added an icon

## 0.1.3

- patch: Add Mercurial support

## 0.1.2

- patch: Upgrade to JDK 11

## 0.1.1

- patch: Added NodeJS support

## 0.1.0

- minor: Initial version of the pipe

